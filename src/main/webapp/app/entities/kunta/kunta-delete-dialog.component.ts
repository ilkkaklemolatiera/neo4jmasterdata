import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IKunta } from 'app/shared/model/kunta.model';
import { KuntaService } from './kunta.service';

@Component({
  templateUrl: './kunta-delete-dialog.component.html',
})
export class KuntaDeleteDialogComponent {
  kunta?: IKunta;

  constructor(protected kuntaService: KuntaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.kuntaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('kuntaListModification');
      this.activeModal.close();
    });
  }
}
