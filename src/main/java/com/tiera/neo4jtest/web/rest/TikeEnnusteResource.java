package com.tiera.neo4jtest.web.rest;

import com.tiera.neo4jtest.domain.TikeEnnuste;
import com.tiera.neo4jtest.service.TikeEnnusteService;
import com.tiera.neo4jtest.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.tiera.neo4jtest.domain.TikeEnnuste}.
 */
@RestController
@RequestMapping("/api")
public class TikeEnnusteResource {

    private final Logger log = LoggerFactory.getLogger(TikeEnnusteResource.class);

    private static final String ENTITY_NAME = "tikeEnnuste";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TikeEnnusteService tikeEnnusteService;

    public TikeEnnusteResource(TikeEnnusteService tikeEnnusteService) {
        this.tikeEnnusteService = tikeEnnusteService;
    }

    /**
     * {@code POST  /tike-ennustes} : Create a new tikeEnnuste.
     *
     * @param tikeEnnuste the tikeEnnuste to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tikeEnnuste, or with status {@code 400 (Bad Request)} if the tikeEnnuste has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tike-ennustes")
    public ResponseEntity<TikeEnnuste> createTikeEnnuste(@Valid @RequestBody TikeEnnuste tikeEnnuste) throws URISyntaxException {
        log.debug("REST request to save TikeEnnuste : {}", tikeEnnuste);
        if (tikeEnnuste.getId() != null) {
            throw new BadRequestAlertException("A new tikeEnnuste cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TikeEnnuste result = tikeEnnusteService.save(tikeEnnuste);
        return ResponseEntity.created(new URI("/api/tike-ennustes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /tike-ennustes} : Updates an existing tikeEnnuste.
     *
     * @param tikeEnnuste the tikeEnnuste to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tikeEnnuste,
     * or with status {@code 400 (Bad Request)} if the tikeEnnuste is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tikeEnnuste couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tike-ennustes")
    public ResponseEntity<TikeEnnuste> updateTikeEnnuste(@Valid @RequestBody TikeEnnuste tikeEnnuste) throws URISyntaxException {
        log.debug("REST request to update TikeEnnuste : {}", tikeEnnuste);
        if (tikeEnnuste.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TikeEnnuste result = tikeEnnusteService.save(tikeEnnuste);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tikeEnnuste.getId()))
            .body(result);
    }

    /**
     * {@code GET  /tike-ennustes} : get all the tikeEnnustes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tikeEnnustes in body.
     */
    @GetMapping("/tike-ennustes")
    public ResponseEntity<List<TikeEnnuste>> getAllTikeEnnustes(Pageable pageable) {
        log.debug("REST request to get a page of TikeEnnustes");
        Page<TikeEnnuste> page = tikeEnnusteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tike-ennustes/:id} : get the "id" tikeEnnuste.
     *
     * @param id the id of the tikeEnnuste to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tikeEnnuste, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tike-ennustes/{id}")
    public ResponseEntity<TikeEnnuste> getTikeEnnuste(@PathVariable String id) {
        log.debug("REST request to get TikeEnnuste : {}", id);
        Optional<TikeEnnuste> tikeEnnuste = tikeEnnusteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tikeEnnuste);
    }

    /**
     * {@code DELETE  /tike-ennustes/:id} : delete the "id" tikeEnnuste.
     *
     * @param id the id of the tikeEnnuste to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tike-ennustes/{id}")
    public ResponseEntity<Void> deleteTikeEnnuste(@PathVariable String id) {
        log.debug("REST request to delete TikeEnnuste : {}", id);
        tikeEnnusteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
