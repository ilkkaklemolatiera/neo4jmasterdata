package com.tiera.neo4jtest.repository;

import com.tiera.neo4jtest.domain.TikeEnnuste;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the TikeEnnuste entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TikeEnnusteRepository extends Neo4jRepository<TikeEnnuste, String> {
}
