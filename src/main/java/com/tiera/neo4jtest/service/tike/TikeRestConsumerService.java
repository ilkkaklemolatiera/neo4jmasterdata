package com.tiera.neo4jtest.service.tike;

import com.tiera.neo4jtest.domain.TikeEnnuste;
import com.tiera.neo4jtest.repository.KuntaRepository;
import com.tiera.neo4jtest.repository.TikeEnnusteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TikeEnnuste}.
 */
@Service
public class TikeRestConsumerService {

    private final Logger log = LoggerFactory.getLogger(TikeRestConsumerService.class);

    @Autowired
    private TikeEnnusteRepository tikeEnnusteRepository;

    @Autowired
    private KuntaRepository kuntaRepository;

    public TikeRestConsumerService() {
    }

    public void getTikeEnnuste() {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String body =  "{\"query\": [{\"code\": \"Alue\",\"selection\": {\"filter\": \"item\", \"values\": [\"KU694\"]}}" +
        "],\"response\": {\"format\": \"json\" }}";

        HttpEntity<String> request = new HttpEntity<String>(body, headers);

        String tikeResourceUrl = "https://pxnet2.stat.fi/PXWeb/api/v1/fi/StatFin/vrm/vaerak/statfin_vaerak_pxt_11rh.px";

        String resultStr = 
        restTemplate.postForObject(tikeResourceUrl, request, String.class);
     
        log.info(resultStr);

        /*
        kuntaRepository.findById("")

        TikeEnnuste tikeEnnuste = new TikeEnnuste();
        tikeEnnuste.setVuosi(2022);
        tikeEnnusteRepository.save(tikeEnnuste);
        */
    }
}
