import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IKunta, Kunta } from 'app/shared/model/kunta.model';
import { KuntaService } from './kunta.service';

@Component({
  selector: 'jhi-kunta-update',
  templateUrl: './kunta-update.component.html',
})
export class KuntaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    kuntakoodi: [null, [Validators.required]],
    nimiFi: [],
    nimiSe: [],
  });

  constructor(protected kuntaService: KuntaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ kunta }) => {
      this.updateForm(kunta);
    });
  }

  updateForm(kunta: IKunta): void {
    this.editForm.patchValue({
      id: kunta.id,
      kuntakoodi: kunta.kuntakoodi,
      nimiFi: kunta.nimiFi,
      nimiSe: kunta.nimiSe,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const kunta = this.createFromForm();
    if (kunta.id !== undefined) {
      this.subscribeToSaveResponse(this.kuntaService.update(kunta));
    } else {
      this.subscribeToSaveResponse(this.kuntaService.create(kunta));
    }
  }

  private createFromForm(): IKunta {
    return {
      ...new Kunta(),
      id: this.editForm.get(['id'])!.value,
      kuntakoodi: this.editForm.get(['kuntakoodi'])!.value,
      nimiFi: this.editForm.get(['nimiFi'])!.value,
      nimiSe: this.editForm.get(['nimiSe'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKunta>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
