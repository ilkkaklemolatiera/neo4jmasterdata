import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'artist',
        loadChildren: () => import('./artist/artist.module').then(m => m.Neo4JmasterdataArtistModule),
      },
      {
        path: 'genre',
        loadChildren: () => import('./genre/genre.module').then(m => m.Neo4JmasterdataGenreModule),
      },
      {
        path: 'track',
        loadChildren: () => import('./track/track.module').then(m => m.Neo4JmasterdataTrackModule),
      },
      {
        path: 'album',
        loadChildren: () => import('./album/album.module').then(m => m.Neo4JmasterdataAlbumModule),
      },
      {
        path: 'kunta',
        loadChildren: () => import('./kunta/kunta.module').then(m => m.Neo4JmasterdataKuntaModule),
      },
      {
        path: 'tike-ennuste',
        loadChildren: () => import('./tike-ennuste/tike-ennuste.module').then(m => m.Neo4JmasterdataTikeEnnusteModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class Neo4JmasterdataEntityModule {}
