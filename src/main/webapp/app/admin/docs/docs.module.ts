import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';

import { DocsComponent } from './docs.component';

import { docsRoute } from './docs.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild([docsRoute])],
  declarations: [DocsComponent],
})
export class DocsModule {}
