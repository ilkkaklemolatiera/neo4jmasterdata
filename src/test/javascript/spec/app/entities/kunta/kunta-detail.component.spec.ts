import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Neo4JmasterdataTestModule } from '../../../test.module';
import { KuntaDetailComponent } from 'app/entities/kunta/kunta-detail.component';
import { Kunta } from 'app/shared/model/kunta.model';

describe('Component Tests', () => {
  describe('Kunta Management Detail Component', () => {
    let comp: KuntaDetailComponent;
    let fixture: ComponentFixture<KuntaDetailComponent>;
    const route = ({ data: of({ kunta: new Kunta('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Neo4JmasterdataTestModule],
        declarations: [KuntaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(KuntaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KuntaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load kunta on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.kunta).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
