package com.tiera.neo4jtest.web.rest;

import com.tiera.neo4jtest.domain.Kunta;
import com.tiera.neo4jtest.repository.KuntaRepository;
import com.tiera.neo4jtest.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.tiera.neo4jtest.domain.Kunta}.
 */
@RestController
@RequestMapping("/api")
public class KuntaResource {

    private final Logger log = LoggerFactory.getLogger(KuntaResource.class);

    private static final String ENTITY_NAME = "kunta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KuntaRepository kuntaRepository;

    public KuntaResource(KuntaRepository kuntaRepository) {
        this.kuntaRepository = kuntaRepository;
    }

    /**
     * {@code POST  /kuntas} : Create a new kunta.
     *
     * @param kunta the kunta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kunta, or with status {@code 400 (Bad Request)} if the kunta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kuntas")
    public ResponseEntity<Kunta> createKunta(@Valid @RequestBody Kunta kunta) throws URISyntaxException {
        log.debug("REST request to save Kunta : {}", kunta);
        if (kunta.getId() != null) {
            throw new BadRequestAlertException("A new kunta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Kunta result = kuntaRepository.save(kunta);
        return ResponseEntity.created(new URI("/api/kuntas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /kuntas} : Updates an existing kunta.
     *
     * @param kunta the kunta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kunta,
     * or with status {@code 400 (Bad Request)} if the kunta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kunta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kuntas")
    public ResponseEntity<Kunta> updateKunta(@Valid @RequestBody Kunta kunta) throws URISyntaxException {
        log.debug("REST request to update Kunta : {}", kunta);
        if (kunta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Kunta result = kuntaRepository.save(kunta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kunta.getId()))
            .body(result);
    }

    /**
     * {@code GET  /kuntas} : get all the kuntas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kuntas in body.
     */
    @GetMapping("/kuntas")
    public ResponseEntity<List<Kunta>> getAllKuntas(Pageable pageable) {
        log.debug("REST request to get a page of Kuntas");
        Page<Kunta> page = kuntaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kuntas/:id} : get the "id" kunta.
     *
     * @param id the id of the kunta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kunta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kuntas/{id}")
    public ResponseEntity<Kunta> getKunta(@PathVariable String id) {
        log.debug("REST request to get Kunta : {}", id);
        Optional<Kunta> kunta = kuntaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(kunta);
    }

    /**
     * {@code DELETE  /kuntas/:id} : delete the "id" kunta.
     *
     * @param id the id of the kunta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kuntas/{id}")
    public ResponseEntity<Void> deleteKunta(@PathVariable String id) {
        log.debug("REST request to delete Kunta : {}", id);
        kuntaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
