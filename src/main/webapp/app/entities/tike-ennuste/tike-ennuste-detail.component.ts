import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITikeEnnuste } from 'app/shared/model/tike-ennuste.model';

@Component({
  selector: 'jhi-tike-ennuste-detail',
  templateUrl: './tike-ennuste-detail.component.html',
})
export class TikeEnnusteDetailComponent implements OnInit {
  tikeEnnuste: ITikeEnnuste | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tikeEnnuste }) => (this.tikeEnnuste = tikeEnnuste));
  }

  previousState(): void {
    window.history.back();
  }
}
