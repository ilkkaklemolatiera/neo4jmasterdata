import { IKunta } from 'app/shared/model/kunta.model';

export interface ITikeEnnuste {
  id?: string;
  vuosi?: number;
  vaesto?: number;
  kunta?: IKunta;
}

export class TikeEnnuste implements ITikeEnnuste {
  constructor(public id?: string, public vuosi?: number, public vaesto?: number, public kunta?: IKunta) {}
}
