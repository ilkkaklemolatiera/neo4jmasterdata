package com.tiera.neo4jtest.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.tiera.neo4jtest.web.rest.TestUtil;

public class KuntaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kunta.class);
        Kunta kunta1 = new Kunta();
        kunta1.setId("id1");
        Kunta kunta2 = new Kunta();
        kunta2.setId(kunta1.getId());
        assertThat(kunta1).isEqualTo(kunta2);
        kunta2.setId("id2");
        assertThat(kunta1).isNotEqualTo(kunta2);
        kunta1.setId(null);
        assertThat(kunta1).isNotEqualTo(kunta2);
    }
}
