import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITikeEnnuste, TikeEnnuste } from 'app/shared/model/tike-ennuste.model';
import { TikeEnnusteService } from './tike-ennuste.service';
import { TikeEnnusteComponent } from './tike-ennuste.component';
import { TikeEnnusteDetailComponent } from './tike-ennuste-detail.component';
import { TikeEnnusteUpdateComponent } from './tike-ennuste-update.component';

@Injectable({ providedIn: 'root' })
export class TikeEnnusteResolve implements Resolve<ITikeEnnuste> {
  constructor(private service: TikeEnnusteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITikeEnnuste> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tikeEnnuste: HttpResponse<TikeEnnuste>) => {
          if (tikeEnnuste.body) {
            return of(tikeEnnuste.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TikeEnnuste());
  }
}

export const tikeEnnusteRoute: Routes = [
  {
    path: '',
    component: TikeEnnusteComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'neo4JmasterdataApp.tikeEnnuste.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TikeEnnusteDetailComponent,
    resolve: {
      tikeEnnuste: TikeEnnusteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.tikeEnnuste.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TikeEnnusteUpdateComponent,
    resolve: {
      tikeEnnuste: TikeEnnusteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.tikeEnnuste.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TikeEnnusteUpdateComponent,
    resolve: {
      tikeEnnuste: TikeEnnusteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.tikeEnnuste.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
