package com.tiera.neo4jtest.service;

import com.tiera.neo4jtest.domain.TikeEnnuste;
import com.tiera.neo4jtest.repository.TikeEnnusteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TikeEnnuste}.
 */
@Service
public class TikeEnnusteService {

    private final Logger log = LoggerFactory.getLogger(TikeEnnusteService.class);

    private final TikeEnnusteRepository tikeEnnusteRepository;

    public TikeEnnusteService(TikeEnnusteRepository tikeEnnusteRepository) {
        this.tikeEnnusteRepository = tikeEnnusteRepository;
    }

    /**
     * Save a tikeEnnuste.
     *
     * @param tikeEnnuste the entity to save.
     * @return the persisted entity.
     */
    public TikeEnnuste save(TikeEnnuste tikeEnnuste) {
        log.debug("Request to save TikeEnnuste : {}", tikeEnnuste);
        return tikeEnnusteRepository.save(tikeEnnuste);
    }

    /**
     * Get all the tikeEnnustes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<TikeEnnuste> findAll(Pageable pageable) {
        log.debug("Request to get all TikeEnnustes");
        return tikeEnnusteRepository.findAll(pageable);
    }


    /**
     * Get one tikeEnnuste by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<TikeEnnuste> findOne(String id) {
        log.debug("Request to get TikeEnnuste : {}", id);
        return tikeEnnusteRepository.findById(id);
    }

    /**
     * Delete the tikeEnnuste by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete TikeEnnuste : {}", id);
        tikeEnnusteRepository.deleteById(id);
    }
}
