import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IKunta } from 'app/shared/model/kunta.model';

type EntityResponseType = HttpResponse<IKunta>;
type EntityArrayResponseType = HttpResponse<IKunta[]>;

@Injectable({ providedIn: 'root' })
export class KuntaService {
  public resourceUrl = SERVER_API_URL + 'api/kuntas';

  constructor(protected http: HttpClient) {}

  create(kunta: IKunta): Observable<EntityResponseType> {
    return this.http.post<IKunta>(this.resourceUrl, kunta, { observe: 'response' });
  }

  update(kunta: IKunta): Observable<EntityResponseType> {
    return this.http.put<IKunta>(this.resourceUrl, kunta, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IKunta>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IKunta[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
