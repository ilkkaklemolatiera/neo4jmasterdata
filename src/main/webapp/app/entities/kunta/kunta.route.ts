import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IKunta, Kunta } from 'app/shared/model/kunta.model';
import { KuntaService } from './kunta.service';
import { KuntaComponent } from './kunta.component';
import { KuntaDetailComponent } from './kunta-detail.component';
import { KuntaUpdateComponent } from './kunta-update.component';

@Injectable({ providedIn: 'root' })
export class KuntaResolve implements Resolve<IKunta> {
  constructor(private service: KuntaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKunta> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((kunta: HttpResponse<Kunta>) => {
          if (kunta.body) {
            return of(kunta.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Kunta());
  }
}

export const kuntaRoute: Routes = [
  {
    path: '',
    component: KuntaComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'neo4JmasterdataApp.kunta.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KuntaDetailComponent,
    resolve: {
      kunta: KuntaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.kunta.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KuntaUpdateComponent,
    resolve: {
      kunta: KuntaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.kunta.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KuntaUpdateComponent,
    resolve: {
      kunta: KuntaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'neo4JmasterdataApp.kunta.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
