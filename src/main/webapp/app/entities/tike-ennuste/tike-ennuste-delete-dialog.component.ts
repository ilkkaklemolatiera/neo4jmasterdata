import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITikeEnnuste } from 'app/shared/model/tike-ennuste.model';
import { TikeEnnusteService } from './tike-ennuste.service';

@Component({
  templateUrl: './tike-ennuste-delete-dialog.component.html',
})
export class TikeEnnusteDeleteDialogComponent {
  tikeEnnuste?: ITikeEnnuste;

  constructor(
    protected tikeEnnusteService: TikeEnnusteService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.tikeEnnusteService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tikeEnnusteListModification');
      this.activeModal.close();
    });
  }
}
