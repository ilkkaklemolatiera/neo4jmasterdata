package com.tiera.neo4jtest.repository;

import com.tiera.neo4jtest.domain.Kunta;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the Kunta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KuntaRepository extends Neo4jRepository<Kunta, String> {
}
