import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';
import { TikeEnnusteComponent } from './tike-ennuste.component';
import { TikeEnnusteDetailComponent } from './tike-ennuste-detail.component';
import { TikeEnnusteUpdateComponent } from './tike-ennuste-update.component';
import { TikeEnnusteDeleteDialogComponent } from './tike-ennuste-delete-dialog.component';
import { tikeEnnusteRoute } from './tike-ennuste.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild(tikeEnnusteRoute)],
  declarations: [TikeEnnusteComponent, TikeEnnusteDetailComponent, TikeEnnusteUpdateComponent, TikeEnnusteDeleteDialogComponent],
  entryComponents: [TikeEnnusteDeleteDialogComponent],
})
export class Neo4JmasterdataTikeEnnusteModule {}
