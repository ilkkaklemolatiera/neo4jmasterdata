package com.tiera.neo4jtest.web.rest;

import com.tiera.neo4jtest.AbstractNeo4jIT;
import com.tiera.neo4jtest.Neo4JmasterdataApp;
import com.tiera.neo4jtest.domain.Kunta;
import com.tiera.neo4jtest.repository.KuntaRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KuntaResource} REST controller.
 */
@SpringBootTest(classes = Neo4JmasterdataApp.class)
@ExtendWith(AbstractNeo4jIT.class)
@AutoConfigureMockMvc
@WithMockUser
public class KuntaResourceIT {

    private static final String DEFAULT_KUNTAKOODI = "AAAAAAAAAA";
    private static final String UPDATED_KUNTAKOODI = "BBBBBBBBBB";

    private static final String DEFAULT_NIMI_FI = "AAAAAAAAAA";
    private static final String UPDATED_NIMI_FI = "BBBBBBBBBB";

    private static final String DEFAULT_NIMI_SE = "AAAAAAAAAA";
    private static final String UPDATED_NIMI_SE = "BBBBBBBBBB";

    @Autowired
    private KuntaRepository kuntaRepository;

    @Autowired
    private MockMvc restKuntaMockMvc;

    private Kunta kunta;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kunta createEntity() {
        Kunta kunta = new Kunta()
            .kuntakoodi(DEFAULT_KUNTAKOODI)
            .nimiFi(DEFAULT_NIMI_FI)
            .nimiSe(DEFAULT_NIMI_SE);
        return kunta;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kunta createUpdatedEntity() {
        Kunta kunta = new Kunta()
            .kuntakoodi(UPDATED_KUNTAKOODI)
            .nimiFi(UPDATED_NIMI_FI)
            .nimiSe(UPDATED_NIMI_SE);
        return kunta;
    }

    @BeforeEach
    public void initTest() {
        kuntaRepository.deleteAll();
        kunta = createEntity();
    }

    @Test
    public void createKunta() throws Exception {
        int databaseSizeBeforeCreate = kuntaRepository.findAll().size();
        // Create the Kunta
        restKuntaMockMvc.perform(post("/api/kuntas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kunta)))
            .andExpect(status().isCreated());

        // Validate the Kunta in the database
        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeCreate + 1);
        Kunta testKunta = kuntaList.get(kuntaList.size() - 1);
        assertThat(testKunta.getKuntakoodi()).isEqualTo(DEFAULT_KUNTAKOODI);
        assertThat(testKunta.getNimiFi()).isEqualTo(DEFAULT_NIMI_FI);
        assertThat(testKunta.getNimiSe()).isEqualTo(DEFAULT_NIMI_SE);
    }

    @Test
    public void createKuntaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kuntaRepository.findAll().size();

        // Create the Kunta with an existing ID
        kunta.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restKuntaMockMvc.perform(post("/api/kuntas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kunta)))
            .andExpect(status().isBadRequest());

        // Validate the Kunta in the database
        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkKuntakoodiIsRequired() throws Exception {
        int databaseSizeBeforeTest = kuntaRepository.findAll().size();
        // set the field null
        kunta.setKuntakoodi(null);

        // Create the Kunta, which fails.


        restKuntaMockMvc.perform(post("/api/kuntas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kunta)))
            .andExpect(status().isBadRequest());

        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllKuntas() throws Exception {
        // Initialize the database
        kuntaRepository.save(kunta);

        // Get all the kuntaList
        restKuntaMockMvc.perform(get("/api/kuntas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))

            .andExpect(jsonPath("$.[*].kuntakoodi").value(hasItem(DEFAULT_KUNTAKOODI)))
            .andExpect(jsonPath("$.[*].nimiFi").value(hasItem(DEFAULT_NIMI_FI)))
            .andExpect(jsonPath("$.[*].nimiSe").value(hasItem(DEFAULT_NIMI_SE)));
    }
    
    @Test
    public void getKunta() throws Exception {
        // Initialize the database
        kuntaRepository.save(kunta);

        // Get the kunta
        restKuntaMockMvc.perform(get("/api/kuntas/{id}", kunta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))

            .andExpect(jsonPath("$.kuntakoodi").value(DEFAULT_KUNTAKOODI))
            .andExpect(jsonPath("$.nimiFi").value(DEFAULT_NIMI_FI))
            .andExpect(jsonPath("$.nimiSe").value(DEFAULT_NIMI_SE));
    }
    @Test
    public void getNonExistingKunta() throws Exception {
        // Get the kunta
        restKuntaMockMvc.perform(get("/api/kuntas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateKunta() throws Exception {
        // Initialize the database
        kuntaRepository.save(kunta);

        int databaseSizeBeforeUpdate = kuntaRepository.findAll().size();

        // Update the kunta
        Kunta updatedKunta = kuntaRepository.findById(kunta.getId()).get();
        updatedKunta
            .kuntakoodi(UPDATED_KUNTAKOODI)
            .nimiFi(UPDATED_NIMI_FI)
            .nimiSe(UPDATED_NIMI_SE);

        restKuntaMockMvc.perform(put("/api/kuntas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedKunta)))
            .andExpect(status().isOk());

        // Validate the Kunta in the database
        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeUpdate);
        Kunta testKunta = kuntaList.get(kuntaList.size() - 1);
        assertThat(testKunta.getKuntakoodi()).isEqualTo(UPDATED_KUNTAKOODI);
        assertThat(testKunta.getNimiFi()).isEqualTo(UPDATED_NIMI_FI);
        assertThat(testKunta.getNimiSe()).isEqualTo(UPDATED_NIMI_SE);
    }

    @Test
    public void updateNonExistingKunta() throws Exception {
        int databaseSizeBeforeUpdate = kuntaRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKuntaMockMvc.perform(put("/api/kuntas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kunta)))
            .andExpect(status().isBadRequest());

        // Validate the Kunta in the database
        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteKunta() throws Exception {
        // Initialize the database
        kuntaRepository.save(kunta);

        int databaseSizeBeforeDelete = kuntaRepository.findAll().size();

        // Delete the kunta
        restKuntaMockMvc.perform(delete("/api/kuntas/{id}", kunta.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kunta> kuntaList = kuntaRepository.findAll();
        assertThat(kuntaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
