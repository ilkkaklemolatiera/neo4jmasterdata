package com.tiera.neo4jtest.domain;

import org.neo4j.springframework.data.core.schema.GeneratedValue;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;
import org.neo4j.springframework.data.core.schema.Property;
import org.neo4j.springframework.data.core.support.UUIDStringGenerator;
import org.neo4j.springframework.data.core.schema.Relationship;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Kunta.
 */
@Node
public class Kunta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(UUIDStringGenerator.class)
    private String id;

    @NotNull
    @Property("kuntakoodi")
    private String kuntakoodi;

    @Property("nimi_fi")
    private String nimiFi;

    @Property("nimi_se")
    private String nimiSe;

    @Relationship
    private Set<TikeEnnuste> tikeennustes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKuntakoodi() {
        return kuntakoodi;
    }

    public Kunta kuntakoodi(String kuntakoodi) {
        this.kuntakoodi = kuntakoodi;
        return this;
    }

    public void setKuntakoodi(String kuntakoodi) {
        this.kuntakoodi = kuntakoodi;
    }

    public String getNimiFi() {
        return nimiFi;
    }

    public Kunta nimiFi(String nimiFi) {
        this.nimiFi = nimiFi;
        return this;
    }

    public void setNimiFi(String nimiFi) {
        this.nimiFi = nimiFi;
    }

    public String getNimiSe() {
        return nimiSe;
    }

    public Kunta nimiSe(String nimiSe) {
        this.nimiSe = nimiSe;
        return this;
    }

    public void setNimiSe(String nimiSe) {
        this.nimiSe = nimiSe;
    }

    public Set<TikeEnnuste> getTikeennustes() {
        return tikeennustes;
    }

    public Kunta tikeennustes(Set<TikeEnnuste> tikeEnnustes) {
        this.tikeennustes = tikeEnnustes;
        return this;
    }

    public Kunta addTikeennuste(TikeEnnuste tikeEnnuste) {
        this.tikeennustes.add(tikeEnnuste);
        tikeEnnuste.setKunta(this);
        return this;
    }

    public Kunta removeTikeennuste(TikeEnnuste tikeEnnuste) {
        this.tikeennustes.remove(tikeEnnuste);
        tikeEnnuste.setKunta(null);
        return this;
    }

    public void setTikeennustes(Set<TikeEnnuste> tikeEnnustes) {
        this.tikeennustes = tikeEnnustes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kunta)) {
            return false;
        }
        return id != null && id.equals(((Kunta) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Kunta{" +
            "id=" + getId() +
            ", kuntakoodi='" + getKuntakoodi() + "'" +
            ", nimiFi='" + getNimiFi() + "'" +
            ", nimiSe='" + getNimiSe() + "'" +
            "}";
    }
}
