import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';

import { AuditsComponent } from './audits.component';

import { auditsRoute } from './audits.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild([auditsRoute])],
  declarations: [AuditsComponent],
})
export class AuditsModule {}
