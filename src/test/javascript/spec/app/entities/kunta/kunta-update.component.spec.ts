import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Neo4JmasterdataTestModule } from '../../../test.module';
import { KuntaUpdateComponent } from 'app/entities/kunta/kunta-update.component';
import { KuntaService } from 'app/entities/kunta/kunta.service';
import { Kunta } from 'app/shared/model/kunta.model';

describe('Component Tests', () => {
  describe('Kunta Management Update Component', () => {
    let comp: KuntaUpdateComponent;
    let fixture: ComponentFixture<KuntaUpdateComponent>;
    let service: KuntaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Neo4JmasterdataTestModule],
        declarations: [KuntaUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(KuntaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KuntaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(KuntaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Kunta('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Kunta();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
