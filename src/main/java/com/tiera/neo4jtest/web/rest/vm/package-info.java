/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tiera.neo4jtest.web.rest.vm;
