import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKunta } from 'app/shared/model/kunta.model';

@Component({
  selector: 'jhi-kunta-detail',
  templateUrl: './kunta-detail.component.html',
})
export class KuntaDetailComponent implements OnInit {
  kunta: IKunta | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ kunta }) => (this.kunta = kunta));
  }

  previousState(): void {
    window.history.back();
  }
}
