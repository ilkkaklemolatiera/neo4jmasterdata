import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';
import { KuntaComponent } from './kunta.component';
import { KuntaDetailComponent } from './kunta-detail.component';
import { KuntaUpdateComponent } from './kunta-update.component';
import { KuntaDeleteDialogComponent } from './kunta-delete-dialog.component';
import { kuntaRoute } from './kunta.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild(kuntaRoute)],
  declarations: [KuntaComponent, KuntaDetailComponent, KuntaUpdateComponent, KuntaDeleteDialogComponent],
  entryComponents: [KuntaDeleteDialogComponent],
})
export class Neo4JmasterdataKuntaModule {}
