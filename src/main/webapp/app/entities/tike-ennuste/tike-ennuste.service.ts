import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITikeEnnuste } from 'app/shared/model/tike-ennuste.model';

type EntityResponseType = HttpResponse<ITikeEnnuste>;
type EntityArrayResponseType = HttpResponse<ITikeEnnuste[]>;

@Injectable({ providedIn: 'root' })
export class TikeEnnusteService {
  public resourceUrl = SERVER_API_URL + 'api/tike-ennustes';

  constructor(protected http: HttpClient) {}

  create(tikeEnnuste: ITikeEnnuste): Observable<EntityResponseType> {
    return this.http.post<ITikeEnnuste>(this.resourceUrl, tikeEnnuste, { observe: 'response' });
  }

  update(tikeEnnuste: ITikeEnnuste): Observable<EntityResponseType> {
    return this.http.put<ITikeEnnuste>(this.resourceUrl, tikeEnnuste, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ITikeEnnuste>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITikeEnnuste[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
