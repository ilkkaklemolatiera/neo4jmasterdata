import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class Neo4JmasterdataHomeModule {}
