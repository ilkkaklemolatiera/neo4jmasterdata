import { ITikeEnnuste } from 'app/shared/model/tike-ennuste.model';

export interface IKunta {
  id?: string;
  kuntakoodi?: string;
  nimiFi?: string;
  nimiSe?: string;
  tikeennustes?: ITikeEnnuste[];
}

export class Kunta implements IKunta {
  constructor(
    public id?: string,
    public kuntakoodi?: string,
    public nimiFi?: string,
    public nimiSe?: string,
    public tikeennustes?: ITikeEnnuste[]
  ) {}
}
