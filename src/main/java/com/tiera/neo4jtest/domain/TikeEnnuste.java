package com.tiera.neo4jtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.neo4j.springframework.data.core.schema.GeneratedValue;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;
import org.neo4j.springframework.data.core.schema.Property;
import org.neo4j.springframework.data.core.support.UUIDStringGenerator;
import org.neo4j.springframework.data.core.schema.Relationship;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A TikeEnnuste.
 */
@Node
public class TikeEnnuste implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(UUIDStringGenerator.class)
    private String id;

    @NotNull
    @Property("vuosi")
    private Integer vuosi;

    @NotNull
    @Property("vaesto")
    private Integer vaesto;

    @Relationship("kunta")
    @JsonIgnoreProperties(value = "tikeennustes", allowSetters = true)
    private Kunta kunta;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVuosi() {
        return vuosi;
    }

    public TikeEnnuste vuosi(Integer vuosi) {
        this.vuosi = vuosi;
        return this;
    }

    public void setVuosi(Integer vuosi) {
        this.vuosi = vuosi;
    }

    public Integer getVaesto() {
        return vaesto;
    }

    public TikeEnnuste vaesto(Integer vaesto) {
        this.vaesto = vaesto;
        return this;
    }

    public void setVaesto(Integer vaesto) {
        this.vaesto = vaesto;
    }

    public Kunta getKunta() {
        return kunta;
    }

    public TikeEnnuste kunta(Kunta kunta) {
        this.kunta = kunta;
        return this;
    }

    public void setKunta(Kunta kunta) {
        this.kunta = kunta;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TikeEnnuste)) {
            return false;
        }
        return id != null && id.equals(((TikeEnnuste) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TikeEnnuste{" +
            "id=" + getId() +
            ", vuosi=" + getVuosi() +
            ", vaesto=" + getVaesto() +
            "}";
    }
}
