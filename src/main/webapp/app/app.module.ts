import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';
import { Neo4JmasterdataCoreModule } from 'app/core/core.module';
import { Neo4JmasterdataAppRoutingModule } from './app-routing.module';
import { Neo4JmasterdataHomeModule } from './home/home.module';
import { Neo4JmasterdataEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    Neo4JmasterdataSharedModule,
    Neo4JmasterdataCoreModule,
    Neo4JmasterdataHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    Neo4JmasterdataEntityModule,
    Neo4JmasterdataAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class Neo4JmasterdataAppModule {}
