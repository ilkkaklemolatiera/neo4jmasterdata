import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Neo4JmasterdataTestModule } from '../../../test.module';
import { TikeEnnusteUpdateComponent } from 'app/entities/tike-ennuste/tike-ennuste-update.component';
import { TikeEnnusteService } from 'app/entities/tike-ennuste/tike-ennuste.service';
import { TikeEnnuste } from 'app/shared/model/tike-ennuste.model';

describe('Component Tests', () => {
  describe('TikeEnnuste Management Update Component', () => {
    let comp: TikeEnnusteUpdateComponent;
    let fixture: ComponentFixture<TikeEnnusteUpdateComponent>;
    let service: TikeEnnusteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Neo4JmasterdataTestModule],
        declarations: [TikeEnnusteUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TikeEnnusteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TikeEnnusteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TikeEnnusteService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TikeEnnuste('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TikeEnnuste();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
