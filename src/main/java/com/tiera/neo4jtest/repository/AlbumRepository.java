package com.tiera.neo4jtest.repository;

import com.tiera.neo4jtest.domain.Album;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the Album entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlbumRepository extends Neo4jRepository<Album, String> {
}
