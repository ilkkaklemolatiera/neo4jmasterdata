package com.tiera.neo4jtest.web.rest;

import com.tiera.neo4jtest.AbstractNeo4jIT;
import com.tiera.neo4jtest.Neo4JmasterdataApp;
import com.tiera.neo4jtest.domain.TikeEnnuste;
import com.tiera.neo4jtest.repository.TikeEnnusteRepository;
import com.tiera.neo4jtest.service.TikeEnnusteService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TikeEnnusteResource} REST controller.
 */
@SpringBootTest(classes = Neo4JmasterdataApp.class)
@ExtendWith(AbstractNeo4jIT.class)
@AutoConfigureMockMvc
@WithMockUser
public class TikeEnnusteResourceIT {

    private static final Integer DEFAULT_VUOSI = 1;
    private static final Integer UPDATED_VUOSI = 2;

    private static final Integer DEFAULT_VAESTO = 1;
    private static final Integer UPDATED_VAESTO = 2;

    @Autowired
    private TikeEnnusteRepository tikeEnnusteRepository;

    @Autowired
    private TikeEnnusteService tikeEnnusteService;

    @Autowired
    private MockMvc restTikeEnnusteMockMvc;

    private TikeEnnuste tikeEnnuste;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TikeEnnuste createEntity() {
        TikeEnnuste tikeEnnuste = new TikeEnnuste()
            .vuosi(DEFAULT_VUOSI)
            .vaesto(DEFAULT_VAESTO);
        return tikeEnnuste;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TikeEnnuste createUpdatedEntity() {
        TikeEnnuste tikeEnnuste = new TikeEnnuste()
            .vuosi(UPDATED_VUOSI)
            .vaesto(UPDATED_VAESTO);
        return tikeEnnuste;
    }

    @BeforeEach
    public void initTest() {
        tikeEnnusteRepository.deleteAll();
        tikeEnnuste = createEntity();
    }

    @Test
    public void createTikeEnnuste() throws Exception {
        int databaseSizeBeforeCreate = tikeEnnusteRepository.findAll().size();
        // Create the TikeEnnuste
        restTikeEnnusteMockMvc.perform(post("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tikeEnnuste)))
            .andExpect(status().isCreated());

        // Validate the TikeEnnuste in the database
        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeCreate + 1);
        TikeEnnuste testTikeEnnuste = tikeEnnusteList.get(tikeEnnusteList.size() - 1);
        assertThat(testTikeEnnuste.getVuosi()).isEqualTo(DEFAULT_VUOSI);
        assertThat(testTikeEnnuste.getVaesto()).isEqualTo(DEFAULT_VAESTO);
    }

    @Test
    public void createTikeEnnusteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tikeEnnusteRepository.findAll().size();

        // Create the TikeEnnuste with an existing ID
        tikeEnnuste.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTikeEnnusteMockMvc.perform(post("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tikeEnnuste)))
            .andExpect(status().isBadRequest());

        // Validate the TikeEnnuste in the database
        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkVuosiIsRequired() throws Exception {
        int databaseSizeBeforeTest = tikeEnnusteRepository.findAll().size();
        // set the field null
        tikeEnnuste.setVuosi(null);

        // Create the TikeEnnuste, which fails.


        restTikeEnnusteMockMvc.perform(post("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tikeEnnuste)))
            .andExpect(status().isBadRequest());

        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkVaestoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tikeEnnusteRepository.findAll().size();
        // set the field null
        tikeEnnuste.setVaesto(null);

        // Create the TikeEnnuste, which fails.


        restTikeEnnusteMockMvc.perform(post("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tikeEnnuste)))
            .andExpect(status().isBadRequest());

        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllTikeEnnustes() throws Exception {
        // Initialize the database
        tikeEnnusteRepository.save(tikeEnnuste);

        // Get all the tikeEnnusteList
        restTikeEnnusteMockMvc.perform(get("/api/tike-ennustes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))

            .andExpect(jsonPath("$.[*].vuosi").value(hasItem(DEFAULT_VUOSI)))
            .andExpect(jsonPath("$.[*].vaesto").value(hasItem(DEFAULT_VAESTO)));
    }
    
    @Test
    public void getTikeEnnuste() throws Exception {
        // Initialize the database
        tikeEnnusteRepository.save(tikeEnnuste);

        // Get the tikeEnnuste
        restTikeEnnusteMockMvc.perform(get("/api/tike-ennustes/{id}", tikeEnnuste.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))

            .andExpect(jsonPath("$.vuosi").value(DEFAULT_VUOSI))
            .andExpect(jsonPath("$.vaesto").value(DEFAULT_VAESTO));
    }
    @Test
    public void getNonExistingTikeEnnuste() throws Exception {
        // Get the tikeEnnuste
        restTikeEnnusteMockMvc.perform(get("/api/tike-ennustes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTikeEnnuste() throws Exception {
        // Initialize the database
        tikeEnnusteService.save(tikeEnnuste);

        int databaseSizeBeforeUpdate = tikeEnnusteRepository.findAll().size();

        // Update the tikeEnnuste
        TikeEnnuste updatedTikeEnnuste = tikeEnnusteRepository.findById(tikeEnnuste.getId()).get();
        updatedTikeEnnuste
            .vuosi(UPDATED_VUOSI)
            .vaesto(UPDATED_VAESTO);

        restTikeEnnusteMockMvc.perform(put("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTikeEnnuste)))
            .andExpect(status().isOk());

        // Validate the TikeEnnuste in the database
        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeUpdate);
        TikeEnnuste testTikeEnnuste = tikeEnnusteList.get(tikeEnnusteList.size() - 1);
        assertThat(testTikeEnnuste.getVuosi()).isEqualTo(UPDATED_VUOSI);
        assertThat(testTikeEnnuste.getVaesto()).isEqualTo(UPDATED_VAESTO);
    }

    @Test
    public void updateNonExistingTikeEnnuste() throws Exception {
        int databaseSizeBeforeUpdate = tikeEnnusteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTikeEnnusteMockMvc.perform(put("/api/tike-ennustes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tikeEnnuste)))
            .andExpect(status().isBadRequest());

        // Validate the TikeEnnuste in the database
        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteTikeEnnuste() throws Exception {
        // Initialize the database
        tikeEnnusteService.save(tikeEnnuste);

        int databaseSizeBeforeDelete = tikeEnnusteRepository.findAll().size();

        // Delete the tikeEnnuste
        restTikeEnnusteMockMvc.perform(delete("/api/tike-ennustes/{id}", tikeEnnuste.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TikeEnnuste> tikeEnnusteList = tikeEnnusteRepository.findAll();
        assertThat(tikeEnnusteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
