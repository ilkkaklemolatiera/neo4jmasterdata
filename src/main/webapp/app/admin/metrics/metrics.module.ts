import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';

import { MetricsComponent } from './metrics.component';

import { metricsRoute } from './metrics.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild([metricsRoute])],
  declarations: [MetricsComponent],
})
export class MetricsModule {}
