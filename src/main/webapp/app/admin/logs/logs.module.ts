import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Neo4JmasterdataSharedModule } from 'app/shared/shared.module';

import { LogsComponent } from './logs.component';

import { logsRoute } from './logs.route';

@NgModule({
  imports: [Neo4JmasterdataSharedModule, RouterModule.forChild([logsRoute])],
  declarations: [LogsComponent],
})
export class LogsModule {}
