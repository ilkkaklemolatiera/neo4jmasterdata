import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITikeEnnuste, TikeEnnuste } from 'app/shared/model/tike-ennuste.model';
import { TikeEnnusteService } from './tike-ennuste.service';
import { IKunta } from 'app/shared/model/kunta.model';
import { KuntaService } from 'app/entities/kunta/kunta.service';

@Component({
  selector: 'jhi-tike-ennuste-update',
  templateUrl: './tike-ennuste-update.component.html',
})
export class TikeEnnusteUpdateComponent implements OnInit {
  isSaving = false;
  kuntas: IKunta[] = [];

  editForm = this.fb.group({
    id: [],
    vuosi: [null, [Validators.required]],
    vaesto: [null, [Validators.required]],
    kunta: [],
  });

  resultjson: any;

  constructor(
    protected tikeEnnusteService: TikeEnnusteService,
    protected kuntaService: KuntaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tikeEnnuste }) => {
      this.updateForm(tikeEnnuste);

      this.kuntaService.query().subscribe((res: HttpResponse<IKunta[]>) => (this.kuntas = res.body || []));
    });
  }

  updateForm(tikeEnnuste: ITikeEnnuste): void {
    this.editForm.patchValue({
      id: tikeEnnuste.id,
      vuosi: tikeEnnuste.vuosi,
      vaesto: tikeEnnuste.vaesto,
      kunta: tikeEnnuste.kunta,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tikeEnnuste = this.createFromForm();
    if (tikeEnnuste.id !== undefined) {
      this.subscribeToSaveResponse(this.tikeEnnusteService.update(tikeEnnuste));
    } else {
      this.subscribeToSaveResponse(this.tikeEnnusteService.create(tikeEnnuste));
    }
  }

  private createFromForm(): ITikeEnnuste {
    return {
      ...new TikeEnnuste(),
      id: this.editForm.get(['id'])!.value,
      vuosi: this.editForm.get(['vuosi'])!.value,
      vaesto: this.editForm.get(['vaesto'])!.value,
      kunta: this.editForm.get(['kunta'])!.value,
    };
  }

  public getTikeStatistic(): void {
    // const body = { a: 1 };

    const body = {
      query: [
        {
          code: 'Alue',
          selection: { filter: 'item', values: ['KU694'] },
        },
      ],
      response: { format: 'json' },
    };

    // this.resultjson = body;

    fetch('https://pxnet2.stat.fi/PXWeb/api/v1/fi/StatFin/vrm/vaerak/statfin_vaerak_pxt_11rh.px', {
      method: 'post',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    })
      .then(res => res.json())
      .then(json => (this.resultjson = json));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITikeEnnuste>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IKunta): any {
    return item.id;
  }
}
