package com.tiera.neo4jtest.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.tiera.neo4jtest.web.rest.TestUtil;

public class TikeEnnusteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TikeEnnuste.class);
        TikeEnnuste tikeEnnuste1 = new TikeEnnuste();
        tikeEnnuste1.setId("id1");
        TikeEnnuste tikeEnnuste2 = new TikeEnnuste();
        tikeEnnuste2.setId(tikeEnnuste1.getId());
        assertThat(tikeEnnuste1).isEqualTo(tikeEnnuste2);
        tikeEnnuste2.setId("id2");
        assertThat(tikeEnnuste1).isNotEqualTo(tikeEnnuste2);
        tikeEnnuste1.setId(null);
        assertThat(tikeEnnuste1).isNotEqualTo(tikeEnnuste2);
    }
}
