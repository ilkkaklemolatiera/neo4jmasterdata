import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Neo4JmasterdataTestModule } from '../../../test.module';
import { TikeEnnusteDetailComponent } from 'app/entities/tike-ennuste/tike-ennuste-detail.component';
import { TikeEnnuste } from 'app/shared/model/tike-ennuste.model';

describe('Component Tests', () => {
  describe('TikeEnnuste Management Detail Component', () => {
    let comp: TikeEnnusteDetailComponent;
    let fixture: ComponentFixture<TikeEnnusteDetailComponent>;
    const route = ({ data: of({ tikeEnnuste: new TikeEnnuste('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Neo4JmasterdataTestModule],
        declarations: [TikeEnnusteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TikeEnnusteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TikeEnnusteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tikeEnnuste on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tikeEnnuste).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
