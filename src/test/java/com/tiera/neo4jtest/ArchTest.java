package com.tiera.neo4jtest;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.tiera.neo4jtest");

        noClasses()
            .that()
                .resideInAnyPackage("com.tiera.neo4jtest.service..")
            .or()
                .resideInAnyPackage("com.tiera.neo4jtest.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.tiera.neo4jtest.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
